﻿using blog.Data;
using blog.Data.Models;
using blog.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Services
{
    public class ArticleService:IArticleService
    {
        private readonly ApplicationDbContext context;
        private readonly ITagService tagService;
        private readonly IMemoryCache cache;
        private TimeSpan span = TimeSpan.FromMinutes(40);

        public ArticleService(ApplicationDbContext context, 
            ITagService tagService,
            IMemoryCache cache)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.tagService = tagService;
            this.cache = cache;
        }

        public async Task<Tuple<int,List<Article>>> ListArticles(int page, int onPage, string search=null)
        {
            string where = "";
            string what = "";
            if (!string.IsNullOrEmpty(search))
            {
                var ind = search.Substring(0, 5);
                what = search.Substring(5);
                if (ind == "_tag_")
                    where = "tag";
                else where = "title";
            }
            var articles = this.context.Articles
                .Include(x => x.CreatedBy)
                .Include(x => x.LastEditedBy)
                .Include(x => x.ArticlesTags)
                    .ThenInclude(at => at.Tag);
            if (where == "title")
            {
                var result =await articles.FirstOrDefaultAsync(a => a.Title == what);
                var list= new List<Article> { result };
                return new Tuple<int, List<Article>>(1, list);
            }
            else
            {
                
                if (where == "tag")
                {
                    var result = articles
                        .Where(a => a.ArticlesTags.Any(at => at.Tag.Name == what));
                    var list=await result
                        .OrderByDescending(x => x.LastEditedOn)
                        .Skip(page*onPage)
                        .Take(onPage)
                        .ToListAsync();
                    var count = await result.CountAsync();
                    return new Tuple<int, List<Article>>(count, list);
                }
                else
                {
                    var count = await articles.CountAsync();
                    var list=await articles
                        .OrderByDescending(x => x.LastEditedOn)
                        .Skip(page * onPage)
                        .Take(onPage)
                        .ToListAsync();
                    return new Tuple<int, List<Article>>(count, list);
                }
            }

        }

        public async Task<List<string>> ListTitles()
        {
            return await this.context.Articles.Select(a=>a.Title).ToListAsync();
        }

        public async Task<Article> FindArticleByTitle(string title)
        {
            return await this.context.Articles.FirstOrDefaultAsync(a => a.Title == title);
        }

        public async Task<Article> FindArticleById(int id)
        {
            return await this.context.Articles
                .Include(a=>a.CreatedBy)
                .Include(a=>a.LastEditedBy)
                .Include(a=>a.ArticlesTags)
                    .ThenInclude(at=>at.Tag)
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<Article>CreateArticle(string title,string content, string userId)
        {
            if (string.IsNullOrEmpty(title) || title.Trim().Length > 20 || title.Trim().Length < 5)
                throw new ArgumentException("The Title must be at least 5 and at max 20 characters long.");
            if (string.IsNullOrEmpty(content) || content.Trim().Length > 1000 || content.Trim().Length < 20)
                throw new ArgumentException("The Title must be at least 20 and at max 1000 characters long.");
            var article = new Article
            {
                Title = title,
                Content=content,
                CreatedById=userId,
                LastEditedById=userId,
                LastEditedOn = DateTime.Now
        };
            this.context.Articles.Add(article);
            await this.context.SaveChangesAsync();
            if (cache.Get("Titles") != null)
            {
                var titles = cache.Get<List<string>>("Titles");
                titles.Add(title);
                cache.Set("Titles", titles,span);
            }
            return article;
        }

        public async Task<Article> EditArticle(
            string title, 
            string content, 
            string userId, 
            int articleId, 
            List<int>tagIds,
            string newTags)
        {
            if (string.IsNullOrEmpty(title)||title.Trim().Length>20||title.Trim().Length<5)
                throw new ArgumentException("The Title must be at least 5 and at max 20 characters long.");
            if (string.IsNullOrEmpty(content) || content.Trim().Length > 1000 || content.Trim().Length < 20)
                throw new ArgumentException("The Title must be at least 20 and at max 1000 characters long.");
            var article = await this.context.Articles.Include(a=>a.ArticlesTags).FirstOrDefaultAsync(a=>a.Id==articleId) ?? throw new ArgumentNullException("Article not found");
            bool HasToSave = false;
            if (article.Title != title)
            {
                HasToSave = true;
                if (cache.Get("Titles") != null)
                {
                    var titles = cache.Get<List<string>>("Titles");
                    titles.Remove(article.Title);
                    titles.Add(title);
                    cache.Set("Titles", titles,span);
                }
                article.Title = title;

            }
            if (article.Content.Trim() != content.Trim())
            {
                HasToSave = true;
                article.Content = content.Trim();
            }
            if ((article.ArticlesTags==null&&tagIds!=null)
                || (article.ArticlesTags != null && tagIds == null)
                ||!article.ArticlesTags.Select(at=>at.TagId).ToList().SequenceEqual(tagIds)
                ||!string.IsNullOrEmpty(newTags))
            {
                HasToSave = true;
                await this.tagService.ChangeTagsOfArticle(tagIds,newTags, articleId);
                
            }
            if (HasToSave)
            {
                article.LastEditedById = userId;
                article.LastEditedOn = DateTime.Now;
                this.context.Articles.Update(article);
                await this.context.SaveChangesAsync();
            }
                return article;
        }
    }
}
