﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using blog.Data;
using blog.Models;
using blog.Services;
using blog.Data.Models;
using blog.Services.Contracts;
using blog.Mappers;
using blog.Areas.Admin.Models;
using blog.Areas.Admin.Mappers;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Data;

namespace blog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IArticleService, ArticleService>();

            services.AddSingleton<IMapper<ArticleViewModel, Article>,ArticleMapper>();
            services.AddSingleton<IMapper<AllUsersViewModel, List<ApplicationUser>>, AllUsersMapper>();
            services.AddSingleton<IMapper<UserViewModel, ApplicationUser>, UserMapper>();
            services.AddSingleton<IMapper<AllArticlesViewModel, List<Article>>, AllArticleMapper>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                   name: "areas",
                   template: "{area:exists}/{controller=Home}/{action=Index}/{page=1}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{page=1}/{id?}");
            });
        }

        public void InitializeDatabase()
        {
                var newDbConnString = Configuration.GetConnectionString("DefaultConnection");
                var connStringBuilder = new SqlConnectionStringBuilder(newDbConnString);
                var newDbName = connStringBuilder.InitialCatalog;

                connStringBuilder.InitialCatalog = "master";

                //create the new DB
                if (!IsDatabaseOnline(newDbConnString))
                {
                    using (var sqlConn = new SqlConnection(connStringBuilder.ToString()))
                    {
                        using (var createDbCmd = sqlConn.CreateCommand())
                        {
                            createDbCmd.CommandText = "CREATE DATABASE " + newDbName;
                            sqlConn.Open();
                            createDbCmd.ExecuteNonQuery();
                        }
                    }

                    //wait up to 30s for the new DB to be fully created
                    //this takes about 4s on my desktop
                    var attempts = 0;
                    var dbOnline = false;
                    while (attempts < 30 && !dbOnline)
                    {
                        if (IsDatabaseOnline(newDbConnString.ToString()))
                        {
                            dbOnline = true;
                        }
                        else
                        {
                            attempts++;
                            Thread.Sleep(1000);
                        }
                    }

                    if (!dbOnline)
                        throw new ApplicationException(string.Format("Waited too long for the newly created database \"{0}\" to come online", newDbName));

                    //apply all migrations
                    //var settings = new DbMigrationsConfiguration();
                    //settings.TargetDatabase = new DbConnectionInfo(newDbConnString);
                    //settings.ContextType = typeof(DbContext);
                    //var dbMigrator = new DbMigrator(settings);
                    //dbMigrator.Update();



                //seed with data

            }
        }
        private bool IsDatabaseOnline(string connString)
        {
            try
            {
                using (var sqlConn = new SqlConnection(connString))
                {
                    sqlConn.Open();
                    return sqlConn.State == ConnectionState.Open;
                }
            }
            catch (SqlException)
            {
                return false;
            }
        }
    }
}
