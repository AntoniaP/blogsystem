﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blog.Controllers;
using blog.Data.Models;
using blog.Mappers;
using blog.Models;
using blog.Services;
using blog.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace blog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ArticleController : Controller
    {
        private readonly ITagService tagService;
        private readonly IArticleService articleService;
        private readonly IMemoryCache cache;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IMapper<ArticleViewModel, Article> articleMapper;
        private TimeSpan span = TimeSpan.FromMinutes(40);






        public ArticleController(ITagService tagService,
            IArticleService articleService,
            UserManager<ApplicationUser> userManager,
            IMapper<ArticleViewModel, Article> articleMapper,
            IMemoryCache cache)
        {
            this.tagService = tagService;
            this.articleService = articleService;
            this.userManager = userManager;
            this.cache = cache;
            this.articleMapper = articleMapper;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        public async Task<IActionResult> CreateArticle()
        {
            if (cache.Get("Tags") == null)
            {
                var tags = await this.tagService.ListAllTags();
                this.cache.Set("Tags", tags,span);
            }
            ViewBag.Tags = cache.Get<List<Tag>>("Tags");
            return View(new ArticleViewModel { Action = "CreateArticle" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateArticle(ArticleViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var tags = this.cache.Get("Tags");
            if (tags == null)
            {
                tags = await this.tagService.ListAllTags();
                this.cache.Set("Tags", tags, span);
            }
            ViewBag.Tags= tags;
            Article article = null;
            if (!string.IsNullOrEmpty(model.Title))
            {
                article = await this.articleService.FindArticleByTitle(model.Title);
                if (article != null)
                {
                    ModelState.AddModelError(string.Empty, $"An article named '{model.Title}' already exists!");
                }
            }
            if (ModelState.IsValid && article==null)
            {
                var userId = userManager.GetUserId(User);
                article = await this.articleService.CreateArticle(model.Title,model.Content,userId);
                await this.tagService.ChangeTagsOfArticle(model.TagIds, model.NewTags, article.Id);
                
                return RedirectToAction("Index", "Home", new {area= ""});
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> UpdateArticle(int id)
        {
            var tags = this.cache.Get("Tags");
            if (tags == null)
            {
                tags=await this.tagService.ListAllTags();
                this.cache.Set("Tags", tags,span);
            }
            ViewBag.Tags = tags;
            var article =await this.articleService.FindArticleById(id);
            var model = this.articleMapper.Map(article);
            model.Action = "UpdateArticle";
            return View("CreateArticle",model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateArticle(ArticleViewModel model,int id)
        {
            Article anotherArticle = null;
            if (!string.IsNullOrEmpty(model.Title))
            {
                anotherArticle = await this.articleService.FindArticleByTitle(model.Title);
                if (anotherArticle != null&&anotherArticle.Id!=id)
                {
                    ModelState.AddModelError(string.Empty, $"An article named '{model.Title}' already exists!");
                }
            }
            var tags = this.cache.Get("Tags");
            if (tags == null)
            {
                tags = await this.tagService.ListAllTags();
                this.cache.Set("Tags", tags, span);
            }
            ViewBag.Tags = tags;
            if (ModelState.IsValid&& (anotherArticle == null || anotherArticle.Id == id))
            {
                
                var article = await this.articleService.EditArticle(
                    model.Title,
                    model.Content,
                    this.userManager.GetUserId(User),
                    id,
                    model.TagIds,
                    model.NewTags);
                return RedirectToAction("Index", "Home", new {area= ""});
            }
            return View("CreateArticle", model);
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}