﻿using blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace blog.Services.Contracts
{
    public interface IArticleService
    {
        Task<Article> FindArticleByTitle(string title);
        Task<Article> FindArticleById(int id);
        Task<Article> CreateArticle(string title, string content, string userId);
        Task<Tuple<int,List<Article>>> ListArticles(int page, int onPage,string search = null);
        Task<Article> EditArticle(
            string title,
            string content,
            string userId,
            int articleId,
            List<int> tagIds,
            string newTags);
        Task<List<string>> ListTitles();
    }
}
