﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Mappers
{
    public interface IMapper<T, U>
    {
        T Map(U entity);
    }
}
