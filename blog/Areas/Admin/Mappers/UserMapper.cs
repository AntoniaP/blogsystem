﻿using blog.Areas.Admin.Models;
using blog.Data.Models;
using blog.Mappers;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Areas.Admin.Mappers
{

        public class UserMapper : IMapper<UserViewModel, ApplicationUser>
        {

        public UserViewModel Map(ApplicationUser entity)
            {
                return new UserViewModel
                {
                    UserName = entity.UserName,
                    Id = entity.Id,
                    Email = entity.Email,
                    IsAdmin=entity.IsAdmin
                };
            }
        }
    
}
