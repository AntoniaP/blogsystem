﻿using blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Areas.Admin.Models
{
    public class AllUsersViewModel
    {
        public List<UserViewModel> Users { get; set; }
        public PagingViewModel PagingModel { get; set; }
        public bool NeedSubmit { get; set; }

    }
}
