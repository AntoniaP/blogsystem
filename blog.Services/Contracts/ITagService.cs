﻿using blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace blog.Services.Contracts
{
    public interface ITagService
    {
        Task<List<Tag>> ListAllTags();
        Task ChangeTagsOfArticle(List<int> tagIds,string newTags, int articleId);
        Task<List<Tag>> CreateTags(string newTags);
    }
}
