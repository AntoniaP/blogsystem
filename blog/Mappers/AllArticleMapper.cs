﻿using blog.Data.Models;
using blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Mappers
{
    public class AllArticleMapper: IMapper <AllArticlesViewModel, List<Article>>
    {
        private IMapper<ArticleViewModel, Article> articleMapper;

        public AllArticleMapper(IMapper<ArticleViewModel, Article> articleMapper)
        {
            this.articleMapper = articleMapper;
        }
        public AllArticlesViewModel Map(List<Article> entity)
        {
            var articleModels = entity.Select(this.articleMapper.Map).ToList();
            return new AllArticlesViewModel { ArticleModels = articleModels };
        }

    }
}
