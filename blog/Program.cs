﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using blog.Data;
using blog.Data.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Configuration;

namespace blog
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = BuildWebHost(args);

            await SeedDatabase(host);

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();






        private static async Task SeedDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                var userManeger = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();





                if (!dbContext.Roles.Any(u => u.Name == "Admin"))
                {
                    await roleManager.CreateAsync(new IdentityRole { Name = "Admin" });
                    ApplicationUser adminUser = null;
                    adminUser = dbContext.Users.FirstOrDefault(u => u.UserName == "Admin");

                    if (adminUser == null)
                    {
                        adminUser = new ApplicationUser { UserName = "Admin", Email = "admin@gmail.com" };
                        await userManeger.CreateAsync(adminUser, "Admin123@");
                    }

                    await userManeger.AddToRoleAsync(adminUser, "Admin");
                }
                IEnumerable<Article> articles = null;
                if (dbContext.Articles.Count() == 0)
                {
                    var articlesJSon = File.ReadAllText(@".\wwwroot\JSons\articles.json");
                    articles = JsonConvert.DeserializeObject<Article[]>(articlesJSon);
                    dbContext.Set<Article>().AddRange(articles);
                }

                if (dbContext.Articles.Count() == 0)
                {
                    var tagsJSon = File.ReadAllText(@".\wwwroot\JSons\tags.json");
                    var tags = JsonConvert.DeserializeObject<Tag[]>(tagsJSon);
                    dbContext.Set<Tag>().AddRange(tags);
                }

                dbContext.SaveChanges();

                if (articles != null)
                {
                    var tagsToAssign = dbContext.Tags.ToList();
                    var adminRoleId = dbContext.Roles.FirstOrDefault(r => r.Name == "Admin").Id;
                    var usersToAssign = dbContext.UserRoles
                        .Where(ur=>ur.RoleId==adminRoleId)
                        .Select(ur=>ur.UserId)
                        .ToList();

                    foreach (var article in articles)
                    {
                        var articlesTags = FindMissingCollection<Tag>(tagsToAssign, 2);
                        var user = FindMissingCollection<string>(usersToAssign, 1).First();
                        article.ArticlesTags = articlesTags
                            .Select(t => new ArticlesTags() { ArticleId = article.Id, TagId = t.Id }).ToList();
                        article.CreatedById = user;
                        article.LastEditedById = user;
                    }

                    dbContext.SaveChanges();
                }
            }

            IEnumerable<U> FindMissingCollection<U>(IList<U> objectsToBeAdded, int counter)
            {
                var rnd = new Random();
                var indexes = new List<int>();
                while (counter > 0)
                {
                    int n = rnd.Next(0, objectsToBeAdded.Count());
                    if (!indexes.Contains(n))
                    {
                        indexes.Add(n);
                        counter--;
                    }
                }
                return indexes.Select(i => objectsToBeAdded[i]);
            }
        }
        
    }
}
