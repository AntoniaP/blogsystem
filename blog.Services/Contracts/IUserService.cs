﻿using blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace blog.Services.Contracts
{
    public interface IUserService
    {
        Task<ApplicationUser> FindUserByName(string name);
        Task<ApplicationUser> SetUserRole(ApplicationUser user);
        Task<Tuple<int, List<ApplicationUser>>> ListUsers(int page, int onPage);
        Task<ApplicationUser> MakeAdmin(string userId);
    }
}
