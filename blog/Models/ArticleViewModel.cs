﻿using blog.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Models
{
    public class ArticleViewModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Title { get; set; }
        public string CreatedById { get; set; }
        public string CreatedByName { get; set; }
        public string LastEditedById { get; set; }
        public string LastEditedByName { get; set; }
        public string NewTags { get; set; }
        [Required]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 20)]
        public string Content { get; set; }
        public List<Tag> Tags { get; set; }
        public List<int> TagIds { get; set; }
        public string Action { get; set; }
        public DateTime LastEditedOn { get; set; }
    }
}
