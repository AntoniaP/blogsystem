﻿using blog.Data;
using blog.Data.Models;
using blog.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blog.Services
{
   public class TagService:ITagService
    {
        private readonly ApplicationDbContext context;
        private readonly IMemoryCache cache;
        private TimeSpan span = TimeSpan.FromMinutes(40);

        public TagService(ApplicationDbContext context, IMemoryCache cache)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.cache = cache;
        }
        public async Task<List<Tag>> ListAllTags()
        {
            return await this.context.Tags.ToListAsync();
        }

        public async Task ChangeTagsOfArticle(List<int>tagIds, string newTags, int articleId)
        {
            var articleTagsToRemove = this.context.ArticlesTags.Where(at => at.ArticleId == articleId);
            var tags = await this.CreateTags(newTags);
            var tagNames = newTags.Split(',').Select(t => t.Trim());
            tags = await this.context.Tags.Where(t => tagNames.Contains(t.Name)).ToListAsync();
            List<int> newTagIds = null;
            if (tags != null)
            {
                if (tagIds != null)
                {
                    newTagIds = tags.Select(t => t.Id).Where(nt => !tagIds.Any(tid => tid == nt)).ToList();
                    tagIds.AddRange(newTagIds);
                }
                else
                {
                    tagIds = tags.Select(t => t.Id).ToList();
                }
            }
            var articleTagsToAdd = tagIds?
                                  .Select(t => new ArticlesTags { ArticleId = articleId, TagId = t });
            if(articleTagsToRemove!=null)
                this.context.ArticlesTags.RemoveRange(articleTagsToRemove);
            if (articleTagsToAdd != null)
                this.context.ArticlesTags.AddRange(articleTagsToAdd);
            if (articleTagsToRemove != null||articleTagsToAdd != null)
                await this.context.SaveChangesAsync();
        }

        public async Task<List<Tag>> CreateTags(string newTags)
        {
            if (!string.IsNullOrEmpty(newTags))
            {
                var tags = newTags.Split(',')
                .Select(t => t.Trim())
                .Where(t => !this.context.Tags.Any(tg => tg.Name == t))
                .Select(t => new Tag { Name = t }).ToList();
                if (tags != null)
                {
                    this.context.Tags.AddRange(tags);
                    await this.context.SaveChangesAsync();
                    var allTags = this.cache.Get<List<Tag>>("Tags");
                    if ( allTags!= null)
                    {
                        allTags.AddRange(tags);
                        this.cache.Set("Tags",allTags,span);
                    }
                    return tags;
                }
                return null;
            }
            return null;
        }
    }
}
