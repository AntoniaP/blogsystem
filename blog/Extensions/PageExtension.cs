﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Extensions
{
    public static class PageExtensions
    {
        public static int GetPages(this int count, int onPage)
        {
            var pages = count / onPage;
            if (pages * onPage < count) pages++;
            return pages;
        }
    }
}
