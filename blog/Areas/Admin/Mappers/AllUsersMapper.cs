﻿using blog.Areas.Admin.Models;
using blog.Data.Models;
using blog.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Areas.Admin.Mappers
{
    public class AllUsersMapper : IMapper<AllUsersViewModel, List<ApplicationUser>>
    {
        private IMapper<UserViewModel, ApplicationUser> userMapper;

        public AllUsersMapper(IMapper<UserViewModel, ApplicationUser> userMapper)
        {
            this.userMapper = userMapper;
        }

        public AllUsersViewModel Map(List<ApplicationUser> entity)
        {
            var userViewModels = entity.Select(userMapper.Map).ToList();
            return new AllUsersViewModel { Users = userViewModels };
        }
    }
}
