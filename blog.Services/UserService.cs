﻿using blog.Data;
using blog.Data.Models;
using blog.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Services
{
    public class UserService:IUserService
    {
        private readonly ApplicationDbContext context;
        //private IServiceProvider serviceProvider;
        private readonly UserManager<ApplicationUser> userManager;

        public UserService(ApplicationDbContext context, UserManager<ApplicationUser> userManager
            )
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            //this.serviceProvider = serviceProvider;
            this.userManager = userManager;
        }
        public async Task<ApplicationUser> FindUserByName(string name)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.UserName == name);
        }

        public async Task<ApplicationUser> SetUserRole(ApplicationUser user)
        {
            if (await userManager.IsInRoleAsync(user, "Admin"))
                user.IsAdmin = true;
            return user;
        }

        public async Task<Tuple<int,List<ApplicationUser>>> ListUsers(int page,int onPage)
        {
            var usersQuery = this.context.Users;
            var users = await usersQuery.Skip(page * onPage).Take(onPage).ToListAsync();
            var count = await usersQuery.CountAsync();
            foreach (var user in users)
            {
                await this.SetUserRole(user);
            }
            return new Tuple<int,List<ApplicationUser>>(count,users);
        }

        public async Task<ApplicationUser> MakeAdmin(string userId)
        {
            var user = await this.context.Users.FindAsync(userId);
            await this.userManager.AddToRoleAsync(user, "Admin");
            return user;
        }
    }
}
