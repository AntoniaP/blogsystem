﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blog.Data.Models;
using blog.Mappers;
using blog.Models;
using blog.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace blog.Controllers
{
    public class ArticleController : Controller
    {
        private IArticleService articleService;
        private IMapper<ArticleViewModel, Article> articleMapper;

        public ArticleController(IArticleService articleService, IMapper<ArticleViewModel, Article> articleMapper)
        {
            this.articleService = articleService;
            this.articleMapper = articleMapper;
        }

        public async Task<IActionResult>Details(int id)
        {
            var article = await this.articleService.FindArticleById(id);
            var model = this.articleMapper.Map(article);
            return View(model);
        }
    }
}