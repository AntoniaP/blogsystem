﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Models
{
    public class PagingViewModel
    {
        public int CurrPage { get; set; }
        public int Pages { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string IdRoute { get; set; }
    }
}
