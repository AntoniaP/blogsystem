﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using blog.Models;
using blog.Services.Contracts;
using blog.Mappers;
using blog.Data.Models;
using Microsoft.Extensions.Caching.Memory;
using blog.Extensions;

namespace blog.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IMemoryCache cache;
        private IArticleService articleService { get; set; }
        private ITagService tagService { get; set; }
        private IMapper<AllArticlesViewModel, List<Article>> allArticlesMapper{ get; set; }

        private const int onPage=3;
        private TimeSpan span = TimeSpan.FromMinutes(40);

        public HomeController(IArticleService articleService,
            ITagService tagService,
            IMapper<AllArticlesViewModel, List<Article>> allArticlesMapper,
            IMemoryCache cache)
        {
            this.articleService = articleService;
            this.tagService = tagService;
            this.allArticlesMapper = allArticlesMapper;
            this.cache = cache;
        }


        public async Task<IActionResult> Index(string id,int page=1)
        {
            List<string> titles = null;
            List<Tag> tags = null;
            ViewBag.MenuBar = "Search";
            
            if (cache.Get("Titles") == null)
            {
                titles =await this.articleService.ListTitles();
                cache.Set("Titles", titles, span);
            }
            if (cache.Get("Tags") == null)
            {
                tags = await this.tagService.ListAllTags();
                this.cache.Set("Tags", tags, span);
            }
            titles = cache.Get<List<string>>("Titles");
            tags = cache.Get<List<Tag>>("Tags");
            var tagNames = tags.Select(t => t.Name).ToList();
            var articles = await this.articleService.ListArticles(page-1,onPage,id);
            AllArticlesViewModel model = new AllArticlesViewModel() ;
            if (articles != null)
            {

                model = this.allArticlesMapper.Map(articles.Item2);
                model.PagingModel = new PagingViewModel()
                {
                    CurrPage = page,
                    Pages = articles.Item1.GetPages(onPage),
                    IdRoute = id,
                };
            }
            model.Tags = tagNames;
            model.Titles = titles;
            model.Test = id;
            
            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
