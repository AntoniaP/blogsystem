﻿using blog.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace blog.Data.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string CreatedById { get; set; }
        public ApplicationUser CreatedBy { get; set; }
        public string LastEditedById { get; set; }
        public ApplicationUser LastEditedBy { get; set; }
        public string Content { get; set; }
        public List<ArticlesTags>ArticlesTags{ get; set; }
        [NotMapped]
        public List<Tag> Tags { get; set; }
        public DateTime LastEditedOn { get; set; }

    }
}
