﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Models
{
    public class AllArticlesViewModel
    {
        public List<ArticleViewModel> ArticleModels { get; set; }
        public List<string> Titles { get; set; }
        public List<string> Tags { get; set; }
        public string Test { get; set; }
        public PagingViewModel PagingModel { get; set; }
    }
}
