﻿using System;
using System.Collections.Generic;
using System.Text;

namespace blog.Data.Models
{
    public class ArticlesTags
    {
        public int ArticleId { get; set; }
        public int TagId { get; set; }
        public Article Article { get; set; }
        public Tag Tag { get; set; }

    }
}
