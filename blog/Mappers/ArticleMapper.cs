﻿using blog.Data.Models;
using blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blog.Mappers
{
    public class ArticleMapper : IMapper<ArticleViewModel, Article>
    {
        public ArticleViewModel Map(Article entity)
        {
            return new ArticleViewModel
            {
                Title = entity.Title,
                Id = entity.Id,
                LastEditedById = entity.LastEditedById,
                LastEditedByName = entity.LastEditedBy.UserName,
                CreatedById = entity.CreatedById,
                CreatedByName = entity.CreatedBy.UserName,
                Content = entity.Content,
                Tags = entity.ArticlesTags?.Select(at => at.Tag).ToList(),
                LastEditedOn=entity.LastEditedOn
            };
        }
    }
}
