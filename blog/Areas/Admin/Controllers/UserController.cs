﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using blog.Areas.Admin.Models;
using blog.Data.Models;
using blog.Extensions;
using blog.Mappers;
using blog.Models;
using blog.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace blog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper<AllUsersViewModel, List<ApplicationUser>> allUsersMapper;
        private const int onPage = 5;

        public UserController(IUserService userService, IMapper<AllUsersViewModel, List<ApplicationUser>> allUsersMapper)
        {
            this.userService = userService;
            this.allUsersMapper = allUsersMapper;
        }

        [HttpGet]
        public async Task<IActionResult> AdminPanel(int page)
        {
            var users = await this.userService.ListUsers(page-1, onPage);
            var model = this.allUsersMapper.Map(users.Item2);
            if (users.Item2.Any(u => !u.IsAdmin)) model.NeedSubmit = true;
            model.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = users.Item1.GetPages(onPage),
                IdRoute = null
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AdminPanel( List<string> newAdmins, int page)
        {
            foreach(var newAdminId in newAdmins)
            {
                await this.userService.MakeAdmin(newAdminId);
            }
            var users = await this.userService.ListUsers(page-1, onPage);
            
            var model = this.allUsersMapper.Map(users.Item2);
            if (users.Item2.Any(u => !u.IsAdmin)) model.NeedSubmit = true;
            model.PagingModel = new PagingViewModel()
            {
                CurrPage = page,
                Pages = users.Item1.GetPages(onPage),
                IdRoute = null
            };
            return View(model);
        }
    }
}